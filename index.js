const cors = require('cors')
const express = require('express')
const app = express()

const mongoose = require('mongoose')
const Form = require('./Schema')
mongoose.connect('mongodb://localhost:27017/myDB')

app.use(cors())
app.use(express.json())


app.get('/',(req,res)=>{
    res.send('<h1>Homepage<h1>')
})

app.post("/submit",(req,res)=>{
    console.log(req.body)

    const newForm = new Form(req.body)
    newForm.save().then(res=>res.send('data recieved successfully...'))

    
})

app.listen(5000, ()=>{
    console.log('Starting server at port 5000 ...')
}) 