const mongoose = require('mongoose')

const Schema = mongoose.Schema({
    name:{type:String, required:true},
    age:{type:String, required:true},
    sex:{type:String, required:true},
    state:{type:String, required:true}})

const Form = mongoose.model('PDF',Schema)

module.export = Form